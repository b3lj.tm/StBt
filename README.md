# strategies_backtester

**StBt** is a backtesting project that contains an algorithmic trading backtester. The backtester is built using the backtrader library and collects datasets using yfinance. The results are then visualized using quantstats, providing an easy-to-use and comprehensive way to evaluate trading strategies. With StBt, users can test their trading algorithms with real-world data and analyze the performance of their strategies using a range of statistical metrics.

## Requirements installing

```bash
pip3 install -r requirements.txt
```

## Import datasets

```bash
./import_data.py
```

```js
dataset/
├── 15m
│   ├── ADA-USD.csv
├── 1d
│   ├── ADA-USD.csv
├── 1h
│   ├── ADA-USD.csv
├── 1m
│   ├── ADA-USD.csv
├── 1mo
│   ├── ADA-USD.csv
├── 1wk
│   ├── ADA-USD.csv
├── 2m
│   ├── ADA-USD.csv
├── 30m
│   ├── ADA-USD.csv
├── 3mo
│   ├── ADA-USD.csv
├── 5d
│   ├── ADA-USD.csv
├── 5m
│   ├── ADA-USD.csv
└── 90m
    ├── ADA-USD.csv
```

## How it works

# Execution

```js
./main.py --help

  ██████ ▄▄▄█████▓ ▄▄▄▄   ▄▄▄█████▓
▒██    ▒ ▓  ██▒ ▓▒▓█████▄ ▓  ██▒ ▓▒
░ ▓██▄   ▒ ▓██░ ▒░▒██▒ ▄██▒ ▓██░ ▒░
  ▒   ██▒░ ▓██▓ ░ ▒██░█▀  ░ ▓██▓ ░
▒██████▒▒  ▒██▒ ░ ░▓█  ▀█▓  ▒██▒ ░
▒ ▒▓▒ ▒ ░  ▒ ░░   ░▒▓███▀▒  ▒ ░░
░ ░▒  ░ ░    ░    ▒░▒   ░     ░
░  ░  ░    ░       ░    ░   ░
      ░            ░
                        ░
Implemented by: https://t.me/b3ljTM

usage: main.py [-h] [--plot [kwargs]] [--fromdate FROMDATE] [--todate TODATE] (--hold_buyAndForget | --hold_buyMoreAndForget)

Strategies Backtrader Script

optional arguments:
  -h, --help            show this help message and exit
  --plot [kwargs]       kwargs in key=value format (default: )
  --fromdate FROMDATE   Date[time] in YYYY-MM-DD[THH:MM:SS] format (default: )
  --todate TODATE       Date[time] in YYYY-MM-DD[THH:MM:SS] format (default: )
  --hold_buyAndForget   Buy and Hold (default: False)
  --hold_buyMoreAndForget
                        Buy and Hold More with Fund ROI (default: False)
```
```js
./main.py --hold_buyMoreAndForget
```
Under results folder you will found the backtesting results.

```js
results/
├── 15m
├── 1d
│   └── Hold_buyMoreAndForget
│       ├── 2021-09-23_23:18:45
│       └── 2021-09-23_23:32:27
│           ├── ADA-EUR.html
│           ├── ADA-USD.html
│           ├── BTC-EUR.html
│           ├── BTC-USD.html
│           ├── EOS-USD.html
│           ├── ETH-EUR.html
│           ├── ETH-USD.html
│           ├── EWT-USD.html
│           └── LTC-USD.html
├── 1h
├── 1m
├── 1mo
├── 1wk
├── 2m
├── 30m
├── 3mo
├── 5d
├── 5m
└── 90m
```

# 0x00 - algorithmic trading - back testing ₿

![image](img/screenshots/00.jpg)

📹 watch demo video

[![0x00 - algorithmic trading - back testing ₿](https://i9.ytimg.com/vi_webp/HNDt4tBT27g/mq2.webp?sqp=CKz6z6IG&rs=AOn4CLD4DhOu7A40wMZcxwOD3tpc5QP8cA)](https://www.youtube.com/watch?v=HNDt4tBT27g)

# 0x01 - algorithmic trading - back testing 🐋 Simple Moving Average SMA(200) Buy and Hold

![image](img/screenshots/ema.png))
![image](img/screenshots/adaeur.png))

📹 watch demo video

[![0x01 - algorithmic trading - back testing 🐋 Simple Moving Average SMA(200) Buy and Hold](https://i9.ytimg.com/vi_webp/FGzk_ivKLus/mq3.webp?sqp=CKz6z6IG&rs=AOn4CLBfReOCwYtwbg0za6u3fbTuHgKtCQ)](https://www.youtube.com/watch?v=FGzk_ivKLus)

# 0x02 - algorithmic trading - back testing 🐋 Exponential Moving Average EMA(50) and EMA(200)

![image](img/screenshots/SMA_EXCEL.png))
![image](img/screenshots/tesla.png))

📹 watch demo video

[![0x02 - algorithmic trading - back testing 🐋 Exponential Moving Average EMA(50) and EMA(200)](https://i9.ytimg.com/vi_webp/fM-6M_q4aI4/mq2.webp?sqp=CKz6z6IG-oaymwEmCMACELQB8quKqQMa8AEB-AHUBoACrAOKAgwIABABGDogXShlMA8=&rs=AOn4CLCxCM_Sen_MgpFqyR-eN2fEWynSdA)](https://www.youtube.com/watch?v=fM-6M_q4aI4)
