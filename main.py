#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM

import argparse
from strategies.ma_buyAndHold import ma_buyAndHold
from strategies.hold_buyMoreAndForget import hold_buyMoreAndForget_strategy
from strategies.hold_buyAndForget import hold_buyAndForget_strategy
from strategies.dummy_RsaStochrsa import dummy_rsaStochrsa_strategy
from strategies.dummy_Ema50Ema200 import dummy_ema50ema200_strategy
from strategies.dummy_Ema50Ema200 import dummy_ema50ema200_strategy
from config import BANNER


def run(args=None):
    args = parse_args(args)

    # Strategy
    if args.hold_buyAndForget:
        hold_buyAndForget_strategy(args.plot)
    elif args.hold_buyMoreAndForget:
        hold_buyMoreAndForget_strategy(args.plot)
    elif args.ma_buyAndHold:
        ma_buyAndHold(args.plot)
    elif args.rsa_stoch_dummy:
        dummy_rsaStochrsa_strategy(args.plot)
    elif args.ema50ema200_dummy:
        dummy_ema50ema200_strategy(args.plot)
    else:
        pass  # Empty else: Misra rule


def parse_args(pargs=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=(
            'Strategies Backtrader Script'
        )
    )
    parser.add_argument('--plot', required=False, default='',
                        nargs='?', const='{}',
                        metavar='kwargs', help='kwargs in key=value format')

    # Defaults for dates
    parser.add_argument('--fromdate', required=False, default='',
                        help='Date[time] in YYYY-MM-DD[THH:MM:SS] format')
    parser.add_argument('--todate', required=False, default='',
                        help='Date[time] in YYYY-MM-DD[THH:MM:SS] format')

    pgroup = parser.add_mutually_exclusive_group(required=True)
    pgroup.add_argument('--hold_buyAndForget', required=False, action='store_true',
                        help='Buy and Hold')
    pgroup.add_argument('--hold_buyMoreAndForget', required=False, action='store_true',
                        help='Buy and Hold More with Fund ROI')
    pgroup.add_argument('--ma_buyAndHold', required=False, action='store_true',
                        help='Moving Average Trading Strategy That Crushes Buy and Hold')
    pgroup.add_argument('--rsa_stoch_dummy', required=False, action='store_true',
                        help='RSA and Stochastic RSA')
    pgroup.add_argument('--ema50ema200_dummy', required=False, action='store_true',
                        help='EMA(200) and EMA(50)')

    return parser.parse_args(pargs)


if __name__ == '__main__':
    print(BANNER)
    parse_args()
    run()
