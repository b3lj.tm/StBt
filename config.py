#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM

BANNER = """\033[2;49;94m
  ██████ ▄▄▄█████▓ ▄▄▄▄   ▄▄▄█████▓
▒██    ▒ ▓  ██▒ ▓▒▓█████▄ ▓  ██▒ ▓▒
░ ▓██▄   ▒ ▓██░ ▒░▒██▒ ▄██▒ ▓██░ ▒░
  ▒   ██▒░ ▓██▓ ░ ▒██░█▀  ░ ▓██▓ ░
▒██████▒▒  ▒██▒ ░ ░▓█  ▀█▓  ▒██▒ ░
▒ ▒▓▒ ▒ ░  ▒ ░░   ░▒▓███▀▒  ▒ ░░
░ ░▒  ░ ░    ░    ▒░▒   ░     ░
░  ░  ░    ░       ░    ░   ░
      ░            ░
                        ░
\033[7;49;96mImplemented by: https://t.me/b3ljTM \033[0;37;40m
"""

VALID_CHARTS_TIME = ["1m", "2m", "5m", "15m", "30m",
                  "90m", "1h", "1d", "5d", "1wk", "1mo", "3mo"]

#TICKERS_LIST = ["ADA-USD", "ADA-EUR", "BTC-USD", "BTC-EUR", "ETH-USD", "ETH-EUR",
#              "EWT-USD", "LTC-USD", "EOS-USD", "MATIC-USD", "DOGE-USD", "XRP-USD"]
TICKERS_LIST = [ "ADA-USD", "ADA-EUR"]
STOCKS_LIST = ["SPY", "TSLA", "AAPL", "GOOG"]

USD_HUNDRED = 100.0
USD_THOUSAND = 1000.0
USD_10_THOUSANDS = 10000.0