#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM
#

from backtrader.indicators import Stochastic, Indicator, MovAv, RelativeStrengthIndex, Highest, Lowest
from config import TICKERS_LIST, USD_THOUSAND
from datetime import datetime   # For datetime objects
import backtrader as bt
import os.path                  # To manage paths
import quantstats


class Dummy_ema50ema200(bt.Strategy):

    params = (
        ('ema50', 50),
        ('ema200', 200),
        ('printlog', False),
    )

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # get start cash
        self.cash_start = self.broker.get_cash()

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        self.ema_50 = bt.indicators.EMA(
            self.datas[0], period=self.params.ema50)
        self.ema_200 = bt.indicators.EMA(
            self.datas[0], period=self.params.ema200)

    def log(self, txt, dt=None, doprint=False):
        # Logging function fot this strategy
        if self.params.printlog or doprint:
            dt = dt or self.datas[0].datetime.date(0)
            print('%s, %s' % (dt.isoformat(), txt))

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                        order.executed.value,
                        order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                             order.executed.value,
                             order.executed.comm))

            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None

    def notify_trade(self, trade):
        if not trade.isclosed:
            return
        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
                 (trade.pnl, trade.pnlcomm))

    def next(self):
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order or not self.ema_200[0] or not self.ema_50[0]:
            return
        else:
            pass
        # Check if we are in the market
        if not self.position:
            if (self.datas[-1].close > self.ema_50[-1]) and (self.datas[-1].close > self.ema_200[-1]):
                # BUY, BUY, BUY!!! (with all possible default parameters)

                # Keep track of the created order to avoid a 2nd order
                target_size = self.broker.get_cash() // self.dataclose[0]
                #print ("target_size   :",target_size)
                self.log('BUY CREATE, %.2f' % self.dataclose[0], doprint=False)
                self.order = self.buy(size=target_size)  # * 0.99)

        elif (self.datas[-1].close < self.ema_50[-1]) and (self.datas[-1].close < self.ema_200[-1]):
            # SELL, SELL, SELL!!! (with all possible default parameters)
            self.log('SELL CREATE, %.2f' % self.dataclose[0], doprint=False)

            # Keep track of the created order to avoid a 2nd order
            self.order = self.close()
        else:
            pass  # empty else

    def stop(self):
        # calculate the actual returns
        self.log('Ending Value %.2f' % (self.broker.getvalue()), doprint=True)
        print(
            f'(Gain: {((((self.broker.getvalue()- self.cash_start)/self.cash_start)) * 100)} %')


def dummy_ema50ema200_strategy(plot=False):
    now = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    result_path = "results/15m/Dummy_ema50ema200/"+now
    if not os.path.isdir(result_path):
        os.makedirs("results/15m/Dummy_ema50ema200/"+now)
    else:
        pass  # empty else

    for ticker in TICKERS_LIST:
        print(f"Ticker : {ticker}")
        cerebro = bt.Cerebro()
        cerebro.broker.set_cash(USD_THOUSAND)
        try:
            data = bt.feeds.YahooFinanceCSVData(
                dataname="./dataset/15m/"+ticker+".csv")
            cerebro.adddata(data)
        except Exception as exp:
            print(f"Deal with it :{exp}")
            print('Try to run import_data.py')
        cerebro.addstrategy(Dummy_ema50ema200)
        cerebro.addanalyzer(bt.analyzers.PyFolio, _name='PyFolio')
        # Set the commission
        cerebro.broker.setcommission(commission=0.001)
        try:
            results = cerebro.run()
            strat = results[0]
            portfolio_stats = strat.analyzers.getbyname('PyFolio')
            returns, positions, transactions, gross_lev = portfolio_stats.get_pf_items()
            returns.index = returns.index.tz_convert(None)
            quantstats.reports.html(
                returns, output=result_path+"/"+ticker+'.html', title=ticker + ' Dummy_ema50ema200')
        except Exception as e:
            print(e)

        if plot:  # Plot if requested to
            cerebro.plot()
        del cerebro
