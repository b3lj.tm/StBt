#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM

import os
from datetime import date, timedelta
import yfinance
from config import TICKERS_LIST, STOCKS_LIST, VALID_CHARTS_TIME

today = date.today().strftime("%Y-%m-%d")
start_1d = date.today() - timedelta(days=729)
start_90m = date.today() - timedelta(days=59)
start_30m = date.today() - timedelta(days=59)
start_15m = date.today() - timedelta(days=59)
start_5m = date.today() - timedelta(days=59)
start_2m = date.today() - timedelta(days=59)
start_1m = date.today() - timedelta(days=7)


def download_yahooFinanceCsvData(currencyCouple):
    pass


for chart_time in VALID_CHARTS_TIME:
    if not os.path.isdir("dataset/"+chart_time):
        os.makedirs("dataset/"+chart_time)
    else:
        pass  # empty elsse

    if not os.path.isdir("results/"+chart_time):
        os.makedirs("results/"+chart_time)
    else:
        pass  # empty elsse


for currencyCouple in TICKERS_LIST:
    # Getting 1m charts
    data = yfinance.download(
        currencyCouple, start=start_1m, end=today, interval="1m")
    data.to_csv("dataset/1m/"+currencyCouple+".csv")
# Getting 2m charts
    data = yfinance.download(
        currencyCouple, start=start_2m, end=today, interval="2m")
    data.to_csv("dataset/2m/"+currencyCouple+".csv")
# Getting 5m charts
    data = yfinance.download(
        currencyCouple, start=start_5m, end=today, interval="5m")
    data.to_csv("dataset/5m/"+currencyCouple+".csv")
# Getting 15m charts
    data = yfinance.download(
        currencyCouple, start=start_15m, end=today, interval="15m")
    data.to_csv("dataset/15m/"+currencyCouple+".csv")
# Getting 30m charts
    data = yfinance.download(
        currencyCouple, start=start_30m, end=today, interval="30m")
    data.to_csv("dataset/30m/"+currencyCouple+".csv")
# Getting 90m charts
    data = yfinance.download(
        currencyCouple, start=start_90m, end=today, interval="90m")
    data.to_csv("dataset/90m/"+currencyCouple+".csv")
# Getting 1h charts
    data = yfinance.download(
        currencyCouple, start=start_1d, end=today, interval="1h")
    data.to_csv("dataset/1h/"+currencyCouple+".csv")
# Getting 1d charts
    data = yfinance.download(
        currencyCouple, start="1900-01-01", end=today, interval="1d")
    data.to_csv("dataset/1d/"+currencyCouple+".csv")
# Getting 5d charts
    data = yfinance.download(
        currencyCouple, start="1900-01-01", end=today, interval="5d")
    data.to_csv("dataset/5d/"+currencyCouple+".csv")
# Getting 1wk charts
    data = yfinance.download(
        currencyCouple, start="1900-01-01", end=today, interval="1wk")
    data.to_csv("dataset/1wk/"+currencyCouple+".csv")
# Getting 1mo charts
    data = yfinance.download(
        currencyCouple, start="1900-01-01", end=today, interval="1mo")
    data.to_csv("dataset/1mo/"+currencyCouple+".csv")
# Getting 3mo charts
    data = yfinance.download(
        currencyCouple, start="1900-01-01", end=today, interval="3mo")
    data.to_csv("dataset/3mo/"+currencyCouple+".csv")

# delete files with no BarDataSeries
# files = os.listdir("data")
# keyword = 'your_keyword'
# for file in files:
#    try:
#        if 1 == sum(1 for line in open(file)):
#            os.close(file)
#            os.system("rm -f dataset/"+file.name)
#    except:
#        pass

# cahnge Datetime to Date
chagneDatetime = "sed -i 's/Datetime/Date/' dataset/*/*csv"
os.system(chagneDatetime)
