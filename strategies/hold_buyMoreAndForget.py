#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM

from config import TICKERS_LIST, USD_HUNDRED, USD_THOUSAND
from datetime import datetime
import backtrader as bt
import os
import quantstats


class Hold_buyMoreAndForget(bt.Strategy):
    params = dict(
        monthly_cash=USD_HUNDRED,  # amount of cash to buy every month
    )

    def start(self):
        self.cash_start = self.broker.get_cash()
        self.val_start = USD_THOUSAND
        # Activate the fund mode and set the default value at BUY_MORE_MONTHLY_CASH
        self.broker.set_fundmode(fundmode=True, fundstartval=self.p.monthly_cash)

        # Add a timer which will be called on the 1st trading day of the month
        self.add_timer(
            bt.timer.SESSION_END,  # when it will be called
            monthdays=[1],  # called on the 1st day of the month
            monthcarry=True,  # called on the 2nd day if the 1st is holiday
        )

    def notify_timer(self, timer, when, *args, **kwargs):
        # Add the influx of monthly cash to the broker
        self.broker.add_cash(self.p.monthly_cash)

        # buy available cash
        target_value = self.broker.get_value() + self.p.monthly_cash
        self.order_target_value(target=target_value)

    def stop(self):
        # calculate the actual returns
        self.roi = (self.broker.get_value() / self.cash_start) - 1.0
        self.froi = self.broker.get_fundvalue() - self.val_start
        print('ROI:        {:.2f}%'.format(100.0 * self.roi))
        print('Fund Value: {:.2f}%'.format(self.froi))


def hold_buyMoreAndForget_strategy(plot=False):
    now = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    result_path = "results/1d/Hold_buyMoreAndForget/"+now
    if not os.path.isdir(result_path):
        os.makedirs("results/1d/Hold_buyMoreAndForget/"+now)
    else:
        pass  # empty else

    for ticker in TICKERS_LIST:
        print(f"Ticker : {ticker}")
        cerebro = bt.Cerebro()
        try:
            data = bt.feeds.YahooFinanceCSVData(
                dataname="./dataset/1d/"+ticker+".csv")
            cerebro.adddata(data)
        except Exception as exp:
            print(f"Deal with it :{exp}")
            print('Try to run import_data.py')

        cerebro.addstrategy(Hold_buyMoreAndForget)
        cerebro.addanalyzer(bt.analyzers.PyFolio, _name='PyFolio')
        try:
            results = cerebro.run()
            strat = results[0]
            portfolio_stats = strat.analyzers.getbyname('PyFolio')
            returns, positions, transactions, gross_lev = portfolio_stats.get_pf_items()
            returns.index = returns.index.tz_convert(None)
            quantstats.reports.html(
                returns, output=result_path+"/"+ticker+'.html', title=ticker + ' Hold_buyMoreAndForget')

        except Exception as e:
            print(e)

        if plot:  # Plot if requested to
            cerebro.plot()
        del cerebro
