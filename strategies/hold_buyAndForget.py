#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM

from config import TICKERS_LIST
from datetime import datetime
import backtrader as bt
import os
import quantstats


class Hold_BuyAndForget(bt.Strategy):
    def start(self):
        self.val_start = self.broker.get_cash()  # keep the starting cash

    def nextstart(self):
        # Buy all the available cash
        self.order_target_value(target=self.broker.get_cash() * .98)

    def stop(self):
        # calculate the actual returns
        self.roi = (self.broker.get_value() / self.val_start) - 1.0
        print('ROI:        {:.2f}%'.format(100.0 * self.roi))


def hold_buyAndForget_strategy(plot=False):

    now = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    result_path = "results/1d/Hold_BuyAndForget/"+now
    if not os.path.isdir(result_path):
        os.makedirs("results/1d/Hold_BuyAndForget/"+now)
    else:
        pass  # empty elsse

    for ticker in TICKERS_LIST:
        print(f"Ticker : {ticker}")
        cerebro = bt.Cerebro()
        try:
            data = bt.feeds.YahooFinanceCSVData(
                dataname="./dataset/1d/"+ticker+".csv")
            cerebro.adddata(data)
        except Exception as exp:
            print(f"Deal with it :{exp}")
            print('Try to run import_data.py')
        cerebro.addstrategy(Hold_BuyAndForget)
        cerebro.addanalyzer(bt.analyzers.PyFolio, _name='PyFolio')

        try:
            results = cerebro.run()
            strat = results[0]
            portfolio_stats = strat.analyzers.getbyname('PyFolio')
            returns, positions, transactions, gross_lev = portfolio_stats.get_pf_items()
            returns.index = returns.index.tz_convert(None)
            quantstats.reports.html(
                returns, output=result_path+"/"+ticker+'.html', title=ticker + ' Hold_BuyAndForget')
        except Exception as e:
            print(e)

        if plot:  # Plot if requested to
            cerebro.plot()
        del cerebro
