#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# @b3ljTM
#

from config import STOCKS_LIST, USD_THOUSAND
from datetime import datetime   # For datetime objects
import backtrader as bt
import os.path                  # To manage paths
import quantstats


class MA_BuyAndHold(bt.Strategy):
    params = (
        ('maperiod', 200),
        ('printlog', False),
    )

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Add a MovingAverageSimple indicator
        self.sma = bt.indicators.SimpleMovingAverage(
            self.datas[0], period=self.params.maperiod)

    def log(self, txt, dt=None, doprint=False):
        ''' Logging function fot this strategy'''
        if self.params.printlog or doprint:
            dt = dt or self.datas[0].datetime.date(0)
            print('%s, %s' % (dt.isoformat(), txt))

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                        order.executed.value,
                        order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                             order.executed.value,
                             order.executed.comm))

            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None

    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('OPERATION PROFIT, GROSS %.2f, NET %.2f' %
                 (trade.pnl, trade.pnlcomm))

    def start(self):
        self.cash_start = self.broker.get_cash()  # keep the starting cash

        # Add a timer which will be called on the 1st trading day of the month
        self.add_timer(
            bt.timer.SESSION_END,  # when it will be called
            monthdays=[-1],  # called on the last day of the month
            monthcarry=True,  # called on the 2nd day if the 1st is holiday
        )

    def notify_timer(self, timer, when, *args, **kwargs):
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
        else:
            pass
        # Check if we are in the market
        if not self.position:
            # Not yet ... we MIGHT BUY if ...
            if self.dataclose[0] > self.sma[0]:
                # BUY, BUY, BUY!!! (with all possible default parameters)

                # Keep track of the created order to avoid a 2nd order
                target_size = self.broker.get_cash() // self.dataclose[0]
                #print ("target_size   :",target_size)
                self.log('BUY CREATE, %.2f' % self.dataclose[0], doprint=True)
                self.order = self.buy(size = target_size * 0.99)
        else:
            if self.dataclose[0] < self.sma[0]:
                # SELL, SELL, SELL!!! (with all possible default parameters)
                self.log('SELL CREATE, %.2f' % self.dataclose[0], doprint=True)

                # Keep track of the created order to avoid a 2nd order
                self.order = self.close()

    def stop(self):
        # calculate the actual returns
        self.log('(MA Period %2d) Ending Value %.2f' %
                 (self.params.maperiod, self.broker.getvalue()+self.broker.get_cash()), doprint=True)
        print(f'(Gain: {((((self.broker.getvalue()+self.broker.get_cash() - self.cash_start)/self.cash_start)) * 100)} %')


def ma_buyAndHold(plot=False):
    now = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    result_path = "results/1d/MA_BuyAndHold/"+now
    if not os.path.isdir(result_path):
        os.makedirs("results/1d/MA_BuyAndHold/"+now)
    else:
        pass  # empty elsse

    for stock in STOCKS_LIST:
        print(f"Ticker : {stock}")
        cerebro = bt.Cerebro()
        cerebro.broker.set_cash(USD_THOUSAND)
        try:
            data = bt.feeds.YahooFinanceCSVData(
                dataname="./dataset/1d/"+stock+".csv")
            cerebro.adddata(data)
        except Exception as exp:
            print(f"Deal with it :{exp}")
            print('Try to run import_data.py')
        cerebro.addstrategy(MA_BuyAndHold)
        cerebro.addanalyzer(bt.analyzers.PyFolio, _name='PyFolio')
        # Set the commission
        cerebro.broker.setcommission(commission=0.001)
        try:
            results = cerebro.run()
            strat = results[0]
            portfolio_stats = strat.analyzers.getbyname('PyFolio')
            returns, positions, transactions, gross_lev = portfolio_stats.get_pf_items()
            returns.index = returns.index.tz_convert(None)
            quantstats.reports.html(
                returns, output=result_path+"/"+stock+'.html', title=stock + ' MA_BuyAndHold')
        except Exception as e:
            print(e)

        if plot:  # Plot if requested to
            cerebro.plot()
        del cerebro
